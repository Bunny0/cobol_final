       IDENTIFICATION DIVISION. 
       PROGRAM-ID. COBOLFINAL.
       AUTHOR SITTHCIHAI TONGPRATEANG.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT INPUT-FILE ASSIGN TO "trader0.dat" 
           ORGANIZATION IS LINE SEQUENTIAL.
       DATA DIVISION. 
       FILE SECTION. 
       FD INPUT-FILE.
       01 INPUT-BUFFER.
           88 END-OF-INPUT-FILE    VALUE HIGH-VALUE.
              05 PROVINCE-ID PIC X(2).
              05 TRADER-P    PIC 9(4).
              05 TRADER-M    PIC 9(6).
       WORKING-STORAGE SECTION. 
       01 TOTAL PIC 9(9) VALUE ZEROES.

       01 MAX-PROVINCE PIC 9(2).
       01 SUM-INCOME   PIC $$$$,$$$,$$$.


       01 RPT-01.
           05 PROVINCE-01       PIC X(2).
           05 MEMBER-INCOME     PIC $$$$,$$$.
           05 TOTAL-MEMBER-01   PIC 9(2).
           05 TOTAL-INCOME-01   PIC $$$$,$$$,$$$.

       01 PRT-HEADER.
           05 FILLER PIC X(8)   VALUE "PROVINCE".
           05 FILLER PIC X(3)   VALUE SPACES.
           05 FILLER PIC X(12)  VALUE " P INCOME   ".
           05 FILLER PIC X(1)   VALUE SPACES.
           05 FILLER PIC X(6)   VALUE "MEMBER".
           05 FILLER PIC X(2)   VALUE SPACES.
           05 FILLER PIC X(13)  VALUE "MEMBER INCOME".

       
       PROCEDURE DIVISION .
       BEGIN.
           OPEN INPUT INPUT-FILE
           DISPLAY PRT-HEADER
           PERFORM READ-LINE
           PERFORM PROCESS-PROV-01 UNTIL END-OF-INPUT-FILE
      *    DISPLAY PRT-FOOTER
           PERFORM PRINT-RESULTS 
           CLOSE INPUT-FILE
           GOBACK
           .

       READ-LINE.
           READ INPUT-FILE
           AT END
              SET END-OF-INPUT-FILE TO TRUE
           END-READ
           .
      
       PRINT-RESULTS.
           DISPLAY "MAX PROVINCE: "
           MAX-PROVINCE
           DISPLAY "SUM INCOME: "
           SUM-INCOME 
           .

       PROCESS-PROV-01.
           MOVE PROVINCE-ID TO PROVINCE-01 
           DISPLAY PROVINCE-01 
           .
           
        







       
   
